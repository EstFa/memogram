package handler.moi.android.fr.memogram;

import java.util.ArrayList;

/**
 *
 * @author dlcgu
 */
public class Etiquette {
    private Etiqu category;
    private String flag;
    private String login_owner;

    Etiquette(Etiqu category,String flag){

        this.category = category;
        this.flag = flag;
    }

    public Etiqu getCategory() {
        return category;
    }

    public void setCategory(Etiqu category) {
        this.category = category;
    }

    public String getFlag() {
        return flag;
    }
    public void set_loginowner(String eti_owner){
        login_owner = eti_owner;
    }

    public String getLogin_owner() {
        return login_owner;
    }

}