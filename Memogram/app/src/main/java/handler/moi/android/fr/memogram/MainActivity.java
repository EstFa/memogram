package handler.moi.android.fr.memogram;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    Button menu1 = null;
    Button menu2 = null;
    public static database new_data = new database();
    public static Utilisateur Jean = new Utilisateur("Jean","mdp",new_data);
    public static Utilisateur Bastien = new Utilisateur("Bastien","mdp",new_data);
    public static String contenu [] = {"Bastien","Coucou ça va ?","Jean","Coucou, bien et toi ?","Bastien","Oui super !","Jean","Pour notre diner de ce soir le code est 15B46","Bastien","Cool merci, j'étiquette vite ça","Jean","Ok, Super à tout à l'heure :)"};
    public static Etiquette etiquettes []= new Etiquette[6];

    private View.OnClickListener clickListenerMessagerie = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent msg_activity = new Intent(MainActivity.this, MessagerieActivity.class);
            startActivity(msg_activity);
        }
    };

    private View.OnClickListener clickListenerEtiquette = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent etiq_activity = new Intent(MainActivity.this, EtiquetteActivity.class);
            startActivity(etiq_activity);
        }
    };

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_messagerie_);
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            int x=0;
            for(int i=0;i<contenu.length/2;i++){
                x=i*2;
                Log.d("donnée","i = "+x);
                if(contenu[i]=="Bastien")
                    Bastien.SendMessage(contenu[x+1], Jean);
                else
                    Jean.SendMessage(contenu[x+1], Bastien);
            }
            Etiqu et []={Etiqu.travail,Etiqu.affaire,Etiqu.drole,Etiqu.personnel,Etiqu.souvenir,Etiqu.none};

            for(int i=0;i <6 ; i++){
                etiquettes[i] = new Etiquette(et[i],"drapeau");
            }

            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.addDrawerListener(toggle);
            toggle.syncState();

            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);

            MainActivity.new_data.getconv2users(MainActivity.Bastien.getLogin(), MainActivity.Jean.getLogin()).getConv().get(0).setEstEtiquete(true,MainActivity.etiquettes[5],MainActivity.Bastien.getLogin());
            MainActivity.new_data.getconv2users(MainActivity.Bastien.getLogin(), MainActivity.Jean.getLogin()).getConv().get(1).setEstEtiquete(true,MainActivity.etiquettes[5],MainActivity.Bastien.getLogin());
            MainActivity.new_data.getconv2users(MainActivity.Bastien.getLogin(), MainActivity.Jean.getLogin()).getConv().get(2).setEstEtiquete(true,MainActivity.etiquettes[5],MainActivity.Bastien.getLogin());
            MainActivity.new_data.getconv2users(MainActivity.Bastien.getLogin(), MainActivity.Jean.getLogin()).getConv().get(3).setEstEtiquete(true,MainActivity.etiquettes[5],MainActivity.Bastien.getLogin());
            MainActivity.new_data.getconv2users(MainActivity.Bastien.getLogin(), MainActivity.Jean.getLogin()).getConv().get(4).setEstEtiquete(true,MainActivity.etiquettes[5],MainActivity.Bastien.getLogin());
            MainActivity.new_data.getconv2users(MainActivity.Bastien.getLogin(), MainActivity.Jean.getLogin()).getConv().get(5).setEstEtiquete(true,MainActivity.etiquettes[5],MainActivity.Bastien.getLogin());

            menu1 = (Button) findViewById(R.id.messagerie);
            menu2 = (Button) findViewById(R.id.etiquette);

            menu1.setOnClickListener(clickListenerMessagerie);
            menu2.setOnClickListener(clickListenerEtiquette);
        }

        @Override
        public void onBackPressed() {
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                super.onBackPressed();
            }
        }

        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.messagerie_, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.
            int id = item.getItemId();

            //noinspection SimplifiableIfStatement
            if (id == R.id.action_settings) {
                Intent setting_activity = new Intent(this, SettingsActivity.class);
                startActivity(setting_activity);
                return true;
            }

            return super.onOptionsItemSelected(item);
        }

        @SuppressWarnings("StatementWithEmptyBody")
        @Override
        public boolean onNavigationItemSelected(MenuItem item) {
            // Handle navigation view item clicks here.
            int id = item.getItemId();

            if (id == R.id.nav_gallery) {
                Intent intent = new Intent(this,MessagerieActivity.class);
                startActivity(intent);

            } else if (id == R.id.nav_slideshow) {
                Intent intent = new Intent(this,EtiquetteActivity.class);
                startActivity(intent);

            } else if (id == R.id.nav_manage) {
                Intent setting_activity = new Intent(this, SettingsActivity.class);
                startActivity(setting_activity);
            }

            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
            return true;
        }

    public static database getNew_data() {
        return new_data;
    }

    public static Utilisateur getJean() {
        return Jean;
    }

    public static Utilisateur getBastien() {
        return Bastien;
    }

    public Etiquette[] getEtiquettes() {
        return etiquettes;
    }
}