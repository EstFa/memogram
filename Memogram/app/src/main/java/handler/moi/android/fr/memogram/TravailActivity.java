package handler.moi.android.fr.memogram;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import static handler.moi.android.fr.memogram.conversation_activity.messages;

public class TravailActivity extends AppCompatActivity {

    String data = conversation_activity.getData();
    TextView v = null;
    TextView v2 = null;
    TextView v3 = null;
    TextView v4 = null;
    TextView v5 = null;
    TextView v6 = null;
    Button b1 = null;

    private LinearLayout myLayout;
    private LinearLayout.LayoutParams paramsdroite;
    private LinearLayout.LayoutParams paramsgauche;


    int choix = PopUp.getChoix();
    int affiche_choix = (int) EtiquetteActivity.getAffiche_etiquette();

    private View.OnClickListener clickListenerIntelligent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intel_activity = new Intent(TravailActivity.this, IntelligentActivity.class);
            startActivity(intel_activity);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inner_etiquette);

        b1 = (Button) findViewById(R.id.but1);
        this.myLayout = (LinearLayout)findViewById(R.id.layout1);

        if(affiche_choix == 1){
            //initialisation des messages
            messages = new ArrayList<>();
            for(int i =0 ; i< MainActivity.getNew_data().get_message_etiqueté(Etiqu.travail).size();i++){
                Button tampon = new Button(this);
                messages.add(tampon);
                //numero_msg=i;
                messages.get(i).setText(MainActivity.getNew_data().get_message_etiqueté(Etiqu.travail).get(i).getContenu());
                messages.get(i).setAllCaps(false);
                if(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(i).getOwner_login()=="Bastien"){
                    messages.get(i).setBackgroundResource(R.drawable.coin_etiquette);
                    myLayout.addView(messages.get(i));
                }
                else if(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(i).getOwner_login()=="Jean"){
                    messages.get(i).setBackgroundResource(R.drawable.coin_autres);
                    myLayout.addView(messages.get(i));
                }
            }
        }

        if(affiche_choix == 2){
            if(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(0).getEtiquette().getCategory() == Etiqu.drole)
            {
                v = (TextView) findViewById(R.id.text1);
                v.setText(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(0).getContenu());
            }
            if(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(1).getEtiquette().getCategory() == Etiqu.drole)
            {
                v2 = (TextView) findViewById(R.id.text2);
                v2.setText(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(1).getContenu());
            }
            if(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(2).getEtiquette().getCategory() == Etiqu.drole)
            {
                v3 = (TextView) findViewById(R.id.text3);
                v3.setText(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(2).getContenu());
            }
            if(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(3).getEtiquette().getCategory() == Etiqu.drole)
            {
                v4 = (TextView) findViewById(R.id.text4);
                v4.setText(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(3).getContenu());
            }
            if(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(4).getEtiquette().getCategory() == Etiqu.drole)
            {
                v5 = (TextView) findViewById(R.id.text5);
                v5.setText(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(4).getContenu());
            }
            if(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(5).getEtiquette().getCategory() == Etiqu.drole)
            {
                v6 = (TextView) findViewById(R.id.text6);
                v6.setText(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(5).getContenu());
            }
        }
        if(affiche_choix == 3){
            if(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(0).getEtiquette().getCategory() == Etiqu.affaire)
            {
                v = (TextView) findViewById(R.id.text1);
                v.setText(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(0).getContenu());
            }
            if(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(1).getEtiquette().getCategory() == Etiqu.affaire)
            {
                v2 = (TextView) findViewById(R.id.text2);
                v2.setText(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(1).getContenu());
            }
            if(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(2).getEtiquette().getCategory() == Etiqu.affaire)
            {
                v3 = (TextView) findViewById(R.id.text3);
                v3.setText(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(2).getContenu());
            }
            if(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(3).getEtiquette().getCategory() == Etiqu.affaire)
            {
                v4 = (TextView) findViewById(R.id.text4);
                v4.setText(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(3).getContenu());
            }
            if(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(4).getEtiquette().getCategory() == Etiqu.affaire)
            {
                v5 = (TextView) findViewById(R.id.text5);
                v5.setText(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(4).getContenu());
            }
            if(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(5).getEtiquette().getCategory() == Etiqu.affaire)
            {
                v6 = (TextView) findViewById(R.id.text6);
                v6.setText(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(5).getContenu());
            }
        }
        if(affiche_choix == 4){
            if(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(0).getEtiquette().getCategory() == Etiqu.personnel)
            {
                v = (TextView) findViewById(R.id.text1);
                v.setText(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(0).getContenu());
            }
            if(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(1).getEtiquette().getCategory() == Etiqu.personnel)
            {
                v2 = (TextView) findViewById(R.id.text2);
                v2.setText(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(1).getContenu());
            }
            if(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(2).getEtiquette().getCategory() == Etiqu.personnel)
            {
                v3 = (TextView) findViewById(R.id.text3);
                v3.setText(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(2).getContenu());
            }
            if(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(3).getEtiquette().getCategory() == Etiqu.personnel)
            {
                v4 = (TextView) findViewById(R.id.text4);
                v4.setText(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(3).getContenu());
            }
            if(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(4).getEtiquette().getCategory() == Etiqu.personnel)
            {
                v5 = (TextView) findViewById(R.id.text5);
                v5.setText(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(4).getContenu());
            }
            if(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(5).getEtiquette().getCategory() == Etiqu.personnel)
            {
                v6 = (TextView) findViewById(R.id.text6);
                v6.setText(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(5).getContenu());
            }
        }
        if(affiche_choix == 5){
            if(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(0).getEtiquette().getCategory() == Etiqu.souvenir)
            {
                v = (TextView) findViewById(R.id.text1);
                v.setText(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(0).getContenu());
            }
            if(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(1).getEtiquette().getCategory() == Etiqu.souvenir)
            {
                v2 = (TextView) findViewById(R.id.text2);
                v2.setText(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(1).getContenu());
            }
            if(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(2).getEtiquette().getCategory() == Etiqu.souvenir)
            {
                v3 = (TextView) findViewById(R.id.text3);
                v3.setText(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(2).getContenu());
            }
            if(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(3).getEtiquette().getCategory() == Etiqu.souvenir)
            {
                v4 = (TextView) findViewById(R.id.text4);
                v4.setText(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(3).getContenu());
            }
            if(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(4).getEtiquette().getCategory() == Etiqu.souvenir)
            {
                v5 = (TextView) findViewById(R.id.text5);
                v5.setText(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(4).getContenu());
            }
            if(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(5).getEtiquette().getCategory() == Etiqu.souvenir)
            {
                v6 = (TextView) findViewById(R.id.text6);
                v6.setText(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(5).getContenu());
            }
        }

        b1.setOnClickListener(clickListenerIntelligent);

    }
}
