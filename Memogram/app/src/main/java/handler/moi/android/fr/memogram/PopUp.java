package handler.moi.android.fr.memogram;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

public class PopUp extends AppCompatActivity {

    Button logo1 = null;
    Button logo2 = null;
    Button logo3 = null;
    Button logo4 = null;
    Button logo5 = null;

    private static int choix_etiquette = 0;

    public static int getChoix(){
        return choix_etiquette;
    }

    private static void init_etiqu()
    {
        MainActivity.etiquettes[5].setCategory(Etiqu.none);
        for(int i=0; i<5 ; i++)
        {
            MainActivity.new_data.getconv2users(MainActivity.Bastien.getLogin(), MainActivity.Jean.getLogin()).getConv().get(i).setEstEtiquete(true,MainActivity.etiquettes[5],MainActivity.Bastien.getLogin());
        }

    }


    private View.OnClickListener clickListenerChoix1 = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            choix_etiquette = 1;
            MainActivity.etiquettes[0].setCategory(Etiqu.travail);
            MainActivity.new_data.getconv2users(MainActivity.Bastien.getLogin(), MainActivity.Jean.getLogin()).getConv().get(conversation_activity.numero_msg).setEstEtiquete(true,MainActivity.etiquettes[0],MainActivity.Bastien.getLogin());
            if(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(conversation_activity.numero_msg).getEtiquette().getCategory() == Etiqu.travail)
            {
                //conversation_activity.test.setText(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(conversation_activity.numero_msg).getContenu());
            }
            PopUp.this.finish();
        }
    };
    private View.OnClickListener clickListenerChoix2 = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            choix_etiquette = 2;
            MainActivity.etiquettes[1].setCategory(Etiqu.drole);
            MainActivity.new_data.getconv2users(MainActivity.Bastien.getLogin(), MainActivity.Jean.getLogin()).getConv().get(conversation_activity.numero_msg).setEstEtiquete(true,MainActivity.etiquettes[1],MainActivity.Bastien.getLogin());
            if(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(conversation_activity.numero_msg).getEtiquette().getCategory() == Etiqu.drole)
            {
                //conversation_activity.test.setText("drole");
                //conversation_activity.test.setText(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(conversation_activity.numero_msg).getContenu());
            }

            PopUp.this.finish();
        }
    };
    private View.OnClickListener clickListenerChoix3 = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            choix_etiquette = 3;
            MainActivity.etiquettes[2].setCategory(Etiqu.affaire);
            MainActivity.new_data.getconv2users(MainActivity.Bastien.getLogin(), MainActivity.Jean.getLogin()).getConv().get(conversation_activity.numero_msg).setEstEtiquete(true,MainActivity.etiquettes[2],MainActivity.Bastien.getLogin());
            if(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(conversation_activity.numero_msg).getEtiquette().getCategory() == Etiqu.affaire)
            {
                //conversation_activity.test.setText("affaire");
            }
            PopUp.this.finish();
        }
    };
    private View.OnClickListener clickListenerChoix4 = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            choix_etiquette = 4;
            MainActivity.etiquettes[3].setCategory(Etiqu.personnel);
            MainActivity.new_data.getconv2users(MainActivity.Bastien.getLogin(), MainActivity.Jean.getLogin()).getConv().get(conversation_activity.numero_msg).setEstEtiquete(true,MainActivity.etiquettes[3],MainActivity.Bastien.getLogin());
            if(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(conversation_activity.numero_msg).getEtiquette().getCategory() == Etiqu.personnel)
            {
                //conversation_activity.test.setText("pers");
            }
            PopUp.this.finish();
        }
    };
    private View.OnClickListener clickListenerChoix5 = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            choix_etiquette = 5;
            MainActivity.etiquettes[4].setCategory(Etiqu.souvenir);
            MainActivity.new_data.getconv2users(MainActivity.Bastien.getLogin(), MainActivity.Jean.getLogin()).getConv().get(conversation_activity.numero_msg).setEstEtiquete(true,MainActivity.etiquettes[4],MainActivity.Bastien.getLogin());
            if(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(conversation_activity.numero_msg).getEtiquette().getCategory() == Etiqu.souvenir)
            {
                //conversation_activity.test.setText("souv");
            }
            PopUp.this.finish();
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Cette directive enlève la barre de titre
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.popupwindow);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        logo1 = (Button) findViewById(R.id.bouton1);
        logo2 = (Button) findViewById(R.id.bouton2);
        logo3 = (Button) findViewById(R.id.bouton3);
        logo4 = (Button) findViewById(R.id.bouton4);
        logo5 = (Button) findViewById(R.id.bouton5);

        logo1.setOnClickListener(clickListenerChoix1);
        logo2.setOnClickListener(clickListenerChoix2);
        logo3.setOnClickListener(clickListenerChoix3);
        logo4.setOnClickListener(clickListenerChoix4);
        logo5.setOnClickListener(clickListenerChoix5);

        getWindow().setLayout((int)(width*.55),(int)(height*.08));
    }
}
