package handler.moi.android.fr.memogram;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MessagerieActivity extends AppCompatActivity {

    private View.OnClickListener clickListenerBoutons = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent conv_activity = new Intent(MessagerieActivity.this, conversation_activity.class);
            startActivity(conv_activity);
        }
    };

    Button b1 = null;
    Button b2 = null;
    Button b3 = null;
    Button b4 = null;
    Button b5 = null;
    Button b6 = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);



        b1 = (Button) findViewById(R.id.button);
        b2 = (Button) findViewById(R.id.button1);
        b3 = (Button) findViewById(R.id.button3);
        b4 = (Button) findViewById(R.id.button4);
        b5 = (Button) findViewById(R.id.button5);
        b6 = (Button) findViewById(R.id.button6);

        b1.setOnClickListener(clickListenerBoutons);
        b2.setOnClickListener(clickListenerBoutons);
        b3.setOnClickListener(clickListenerBoutons);
        b4.setOnClickListener(clickListenerBoutons);
        b5.setOnClickListener(clickListenerBoutons);
        b6.setOnClickListener(clickListenerBoutons);
    }



}
