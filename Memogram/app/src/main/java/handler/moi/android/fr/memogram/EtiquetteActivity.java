package handler.moi.android.fr.memogram;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class EtiquetteActivity extends AppCompatActivity {

    int choix = PopUp.getChoix();
    private static int affiche_etiquette = 0;
    public static int getAffiche_etiquette(){return  affiche_etiquette;}

    private View.OnClickListener clickListenerBouton = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            switch (v.getId()){
                case  R.id.button :
                    affiche_etiquette = 1;
                    Intent innerEtiquette_activity = new Intent(EtiquetteActivity.this, TravailActivity.class);
                    startActivity(innerEtiquette_activity);
                    break;

                case R.id.button1 :
                    affiche_etiquette = 2;
                    Intent innerEtiquette_activity2 = new Intent(EtiquetteActivity.this, TravailActivity.class);
                    startActivity(innerEtiquette_activity2);
                    break;

                case R.id.button2 :
                    affiche_etiquette = 3;
                    Intent innerEtiquette_activity3 = new Intent(EtiquetteActivity.this, TravailActivity.class);
                    startActivity(innerEtiquette_activity3);
                    break;

                case R.id.button3 :
                    affiche_etiquette = 4;
                    Intent innerEtiquette_activity4 = new Intent(EtiquetteActivity.this, TravailActivity.class);
                    startActivity(innerEtiquette_activity4);
                    break;

                case R.id.button4 :
                    affiche_etiquette = 5;
                    Intent innerEtiquette_activity5 = new Intent(EtiquetteActivity.this, TravailActivity.class);
                    startActivity(innerEtiquette_activity5);
                    break;
            }
        }
    };

    private View.OnClickListener clickListenerBouton2 = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };

    private View.OnClickListener clickListenerBouton3 = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent innerEtiquette_activity = new Intent(EtiquetteActivity.this, TravailActivity.class);
            startActivity(innerEtiquette_activity);
        }
    };

    private View.OnClickListener clickListenerBouton4 = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent innerEtiquette_activity = new Intent(EtiquetteActivity.this, TravailActivity.class);
            startActivity(innerEtiquette_activity);
        }
    };

    private View.OnClickListener clickListenerBouton5 = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent innerEtiquette_activity = new Intent(EtiquetteActivity.this, TravailActivity.class);
            startActivity(innerEtiquette_activity);
        }
    };

    Button b1 = null;
    Button b2 = null;
    Button b3 = null;
    Button b4 = null;
    Button b5 = null;
    Button b6 = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.etiquette);

        b1 = (Button) findViewById(R.id.button);
        b2 = (Button) findViewById(R.id.button1);
        b3 = (Button) findViewById(R.id.button2);
        b4 = (Button) findViewById(R.id.button3);
        b5 = (Button) findViewById(R.id.button4);
        b6 = (Button) findViewById(R.id.button6);

        b1.setOnClickListener(clickListenerBouton);
        b2.setOnClickListener(clickListenerBouton);
        b3.setOnClickListener(clickListenerBouton);
        b4.setOnClickListener(clickListenerBouton);
        b5.setOnClickListener(clickListenerBouton);
        b6.setOnClickListener(clickListenerBouton);
    }
}
