package handler.moi.android.fr.memogram;

import java.util.ArrayList;

/**
 *
 * @author dlcgu
 */
public class database {
    private ArrayList <Conversation> allConv;
    private ArrayList <Utilisateur> allUsers;

    database(){
        allConv = new ArrayList();
        allUsers = new ArrayList();
    }

    ///Getter and Setter
    public ArrayList<Conversation> getAllConv() {
        return allConv;
    }

    public void setAllConv(ArrayList<Conversation> allConv) {
        this.allConv = allConv;
    }

    public ArrayList<Utilisateur> getAllUsers() {
        return allUsers;
    }

    public void setAllUsers(ArrayList<Utilisateur> allUsers) {
        this.allUsers = allUsers;
    }

    //Methode
    public boolean User_exist(String login){
        for(int i=0 ; i<allUsers.size();i++){
            if(login == allUsers.get(i).getLogin())
                return true;
        }
        return false;
    }

    //ajout utilisateur à la database
    public void add_User(Utilisateur user){
        allUsers.add(user);
    }

    //ajout conversation à la database
    public void add_Conv(Conversation conv){
        allConv.add(conv);
    }

    public Conversation get_conv_by_Id(String conv_id){
        for(int i=0 ; i<allConv.size();i++){
            if(conv_id == allConv.get(i).getClef())
                return allConv.get(i);
        }
        return null;
    }

    //retourne toute les conversation d'un utilisateur
    public ArrayList <Conversation> getmyConvs(String login){
        ArrayList <Conversation> conv = new ArrayList();
        for(int i=0 ; i<allConv.size();i++){
            if(login == allConv.get(i).getUser1().getLogin() || login == allConv.get(i).getUser2().getLogin() )
                conv.add(allConv.get(i));
        }
        return conv;
    }

    //boolean pour savoir si une conversation existe entre deux users
    public boolean conv_exist(String login1, String login2){
        for(int i=0 ; i<allConv.size();i++){
            if((login1 == allConv.get(i).getUser1().getLogin() && login2 == allConv.get(i).getUser2().getLogin())||(login1 == allConv.get(i).getUser2().getLogin() && login2 == allConv.get(i).getUser1().getLogin()))
                return true;
        }
        return false;
    }

    //Récupérer la conversation qui existe entre deux users
    public Conversation getconv2users(String login1, String login2){
        for(int i=0 ; i<allConv.size();i++){
            if((login1 == allConv.get(i).getUser1().getLogin() && login2 == allConv.get(i).getUser2().getLogin())||(login1 == allConv.get(i).getUser2().getLogin() && login2 == allConv.get(i).getUser1().getLogin()))
                return allConv.get(i);
        }
        return null;
    }

    void nb_conv(){
        System.out.println(allConv.size());
    }
    void nb_users(){
        System.out.println(allUsers.size());
    }

    //Récupère tous les messages assignés à une étiquette
    ArrayList <message> get_message_etiqueté(Etiqu category){
        ArrayList <message> mess_eti = new ArrayList();
        for(int i=0 ; i<allConv.size();i++)
            for(int j=0 ; j<allConv.get(i).getConv().size();j++)
                if(allConv.get(i).getConv().get(i).getestEtiquete())
                    if(allConv.get(i).getConv().get(i).getEtiquette().getCategory()== category)
                        mess_eti.add(allConv.get(i).getConv().get(i));
        return mess_eti;
    }

}