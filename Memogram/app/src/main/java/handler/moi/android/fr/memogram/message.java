package handler.moi.android.fr.memogram;


import java.util.Date;
import java.util.UUID;

public class message {
    private Date date;
    private boolean estEtiquete;
    private Etiquette etiquette;
    private String contenu;
    private String owner_login;
    private String message_key;

    //Constructeur surchargé
    message(String contenu, String owner_login){
        date = new Date();
        this.contenu = contenu;
        estEtiquete = false;
        this.owner_login = owner_login;
        message_key = UUID.randomUUID().toString();
    }
    //getter date
    Date getDate(){
        return date;
    }
    //getter boolean étiquetage
    boolean getestEtiquete(){
        return estEtiquete;
    }
    /*assigner etiquette à un message
    attributs : - boolean estEtiquete : rendre actif l'etiquetage
                - Etiquette etiquette : etiquette que l'on veut assigner
                - login_owner : propriétaire de l'étiquette
    */
    public void setEstEtiquete(boolean estEtiquete,Etiquette etiquette, String login_owner) {
        this.etiquette = etiquette;
        this.estEtiquete = estEtiquete;
        this.etiquette.set_loginowner(login_owner);

    }
    public String getOwner_login(){
        return owner_login;
    }

    public String getContenu() {
        return contenu;
    }

    public Etiquette getEtiquette() {
        return etiquette;
    }

}