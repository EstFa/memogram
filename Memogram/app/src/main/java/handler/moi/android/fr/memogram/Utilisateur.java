package handler.moi.android.fr.memogram;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class Utilisateur {
    private String login;
    private String password;
    protected database data;

    Utilisateur(String login,String password,database newdata){
        this.login = login;
        this.password = password;
        data = newdata;
        data.add_User(this);
    }

    public String getLogin() {
        return login;
    }

    boolean VerifIdentifiant(String password, String login){
        if(password == this.password && login == this.login){
            return true;
        }else
            return false;
    }

    //Création d'un nouveau message et assigner à une conversation
    void SendMessage(String contenu, Utilisateur user){
        message new_message = new message(contenu,this.login);
        getConv(user).set_conv(new_message);
    }

    //Verification de l'existence d'une conversation et recupération de la conversation
    Conversation getConv(Utilisateur user) {
        if(data.conv_exist(this.login, user.getLogin()))
            return data.getconv2users(this.login, user.getLogin());
        else{
            Conversation newconv = new Conversation(this,user);
            data.add_Conv(newconv);
            return newconv;
        }
    }

    void affiche(){
        data.nb_conv();
        data.nb_users();
    }

}

