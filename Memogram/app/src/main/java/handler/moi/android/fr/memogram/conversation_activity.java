package handler.moi.android.fr.memogram;

import android.content.Intent;
import android.os.TestLooperManager;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.EditText;

import java.util.ArrayList;

public class conversation_activity extends AppCompatActivity {

    private static String data = "";
    private LinearLayout myLayout;
    private LinearLayout.LayoutParams paramsdroite;
    private LinearLayout.LayoutParams paramsgauche;
    public static ArrayList<Button> messages;
    private TextInputEditText messagetoSend;
    private Button send;
    private TextView destinataire;



    public static int numero_msg=100;
    public  static EditText test = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.conversation);
        this.myLayout = (LinearLayout) findViewById(R.id.Layoutdynamique);
        this.messagetoSend = (TextInputEditText) findViewById(R.id.messagetosend);
        this.destinataire = (TextView) findViewById(R.id.destinataire);
        this.send = (Button) findViewById(R.id.sendButton);



        //test = (EditText) findViewById(R.id.temp);

        this.destinataire.setText(MainActivity.getJean().getLogin());

        send.setOnClickListener(SendMessage);
        paramsdroite = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT,1);
        paramsdroite.setMargins(250,0,0,0);
        paramsgauche = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT,1);
        paramsgauche.setMargins(0,0,250,0);

        //initialisation des messages
        messages = new ArrayList<>();
        for(int i =0 ; i<MainActivity.getBastien().getConv(MainActivity.getJean()).get_nb_messages();i++){
            Button tampon = new Button(this);
            messages.add(tampon);
            //numero_msg=i;
            messages.get(i).setOnClickListener(clickListenerPopUp);
            messages.get(i).setText(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(i).getContenu());
            messages.get(i).setAllCaps(false);
            if(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(i).getOwner_login().equals("Bastien")){
                messages.get(i).setBackgroundResource(R.drawable.coin_etiquette);
                myLayout.addView(messages.get(i), paramsdroite);
            }
            else if(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(i).getOwner_login().equals("Jean")){
                messages.get(i).setBackgroundResource(R.drawable.coin_autres);
                myLayout.addView(messages.get(i), paramsgauche);
            }
        }
    }
    public static String getData(){
        return data;

    }

    private View.OnClickListener clickListenerPopUp = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            /* Réagir au toucher pour le bouton*/
            Button msg = (Button)v;

            if(msg.getText()== MainActivity.contenu[1]) { numero_msg = 0; }
            if(msg.getText()== MainActivity.contenu[3]) { numero_msg = 1; }
            if(msg.getText()== MainActivity.contenu[5]) { numero_msg = 2; }
            if(msg.getText()== MainActivity.contenu[7]) { numero_msg = 3; }
            if(msg.getText()== MainActivity.contenu[9]) { numero_msg = 4; }
            if(msg.getText()== MainActivity.contenu[11]) { numero_msg = 5; }

//            test.setText(""+numero_msg);
            //ArrayList<Button> msg = (ArrayList<Button>) v;
            Intent popup = new Intent(conversation_activity.this, PopUp.class);
            startActivity(popup);
        }
    };
    private void Sendmessage(){
        if(messagetoSend.getText().toString()!="...") {
            MainActivity.getBastien().SendMessage(messagetoSend.getText().toString(), MainActivity.getJean());
            Button tampon = new Button(this);
            messages.add(tampon);
            messages.get(messages.size() - 1).setOnClickListener(clickListenerPopUp);
            messages.get(messages.size() - 1).setText(MainActivity.getBastien().getConv(MainActivity.getJean()).getConv().get(messages.size() - 1).getContenu());
            messages.get(messages.size() - 1).setAllCaps(false);
            messages.get(messages.size() - 1).setBackgroundResource(R.drawable.coin_etiquette);
            myLayout.addView(messages.get(messages.size() - 1), paramsdroite);
            messagetoSend.setText("...");
        }
    }
    private View.OnClickListener SendMessage = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Sendmessage();
        }
    };
}
