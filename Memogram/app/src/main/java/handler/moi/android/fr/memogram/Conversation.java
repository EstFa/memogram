package handler.moi.android.fr.memogram;


/**
 *
 * @author dlcgu
 */
import java.util.ArrayList;
import java.util.UUID;

public class Conversation {

    private Utilisateur user1;
    private Utilisateur user2;
    private String clef;
    private ArrayList <message> conv = new ArrayList<message>();


    Conversation(Utilisateur user1,Utilisateur user2){

        this.user1 = user1;
        this.user2 = user2;
        //création d'une clef unique pour la conversation
        clef = UUID.randomUUID().toString();
    }

    public Utilisateur getUser1() {
        return user1;
    }

    public void setUser1(Utilisateur user1) {
        this.user1 = user1;
    }

    public Utilisateur getUser2() {
        return user2;
    }

    public void setUser2(Utilisateur user2) {
        this.user2 = user2;
    }

    public String getClef() {
        return clef;
    }

    public void setClef(String clef) {
        this.clef = clef;
    }
    public void  set_conv(message sms){
        conv.add(sms);
    }

    //affichage console
    public void affichage_Conv(){
        System.out.println("nb message:"+conv.size());
        for(int i=0;i<conv.size();i++){
            System.out.println(conv.get(i).getOwner_login()+" :"+conv.get(i).getContenu());
        }
    }

    public ArrayList<message> getConv() {
        return conv;
    }

    public int get_nb_messages(){
        return conv.size();
    }

}


